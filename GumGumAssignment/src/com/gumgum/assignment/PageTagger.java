package com.gumgum.assignment;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class PageTagger {
	String modelFile = "/Users/abhishekashwathnarayanvenkat/Documents/homeWs/GumGumAssignment/src/lib/english-left3words-distsim.tagger";
	MaxentTagger mt = new MaxentTagger(modelFile);
	public static void main(String[] args) {
		PageTagger pt = new PageTagger();
		// String pages[] = { "http://gumgum.com/" };
		String pages[] = { "http://gumgum.com/", "http://www.popcrunch.com/jimmy-kimmel-engaged/",
				"http://www.windingroad.com/articles/reviews/quick-drive-2012-bmw-z4-sdrive28i/",
				"http://gumgum-public.s3.amazonaws.com/numbers.html" };
		for (String string : pages) {
			String parsedText = pt.getText(string);
			// System.out.println(parsedText);
			System.out.println(pt.tagText(parsedText));
		}
	}

	/**
	 * Using MaxentTagger class of the POStagger library, it will tag the input
	 * string
	 * 
	 * @param input
	 * @return
	 */
	public String tagText(String input) {
		
		String output = null;
		output = mt.tagString(input);
		return output;
	}

	/**
	 * parses the HTML and extract words in the body as a string
	 * 
	 * @param inputURL
	 * @return
	 */
	public String getText(String inputURL) {
		String output = null;
		try {
			Document doc;
			doc = Jsoup.connect(inputURL).get();
			output = doc.body().text();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// output = bodyText.attr("body:src");
		return output;
	}
}
